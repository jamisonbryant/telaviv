﻿# Credits

## Development

* **Lead Developer** - Jamison Bryant (https://gitlab.com/jamisonbryant)

## Assets

* Silk Icons by Mark James (http://www.famfamfam.com/lab/icons/silk)