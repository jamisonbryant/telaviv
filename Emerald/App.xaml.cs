﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Emerald
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Log file
        /// </summary>
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Application name
        /// </summary>
        public static string AppName = "Emerald";

        /// <summary>
        /// Full path to application data directory
        /// </summary>
        public static string DataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + 
            Path.DirectorySeparatorChar + AppName + Path.DirectorySeparatorChar + "data";

        /// <summary>
        /// Full path to application user data directory
        /// </summary>
        public static string UserDataDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + 
            Path.DirectorySeparatorChar + AppName;

        /// <summary>
        /// Major version number
        /// </summary>
        public static int MajorVersion = 0;

        /// <summary>
        /// Minor version number
        /// </summary>
        public static int MinorVersion = 1;

        /// <summary>
        /// Patch number
        /// </summary>
        public static int PatchNumber = 0;

        /// <summary>
        /// Release tag
        /// </summary>
        public static string ReleaseTag = "prealpha";

        public App() { }

        /// <summary>
        /// Initializes the application.
        /// </summary>
        private void Initialize(object sender, StartupEventArgs e)
        {
            // Initialize logging
            log4net.Config.XmlConfigurator.Configure();

            // Create app data directories
            // TODO: Refactor as "load data dirs"
            CreateDataDirs();

            // Create flat-file databases
            // TODO: Refactor as "load database"
            CreateDatabase();

            // Load application settings
            LoadConfiguration();

            // Initialize window objects
            // We will use one or the other of these in a moment, depending on what
            // command-line arguments were passed.
            //MainWindow m = new MainWindow();
            LoginWindow l = new LoginWindow();

            // Process command-line arguments
            //if (e.Args.Length > 0) {
            //    for (int i = 0; i < e.Args.Length; i++) {
            //        // If the --bypass-login argument is passed, then skip the login window and go
            //        // directly to the main window. This is used for testing purposes, when repetetive
            //        // login is not desired.
            //        if (e.Args[i].Equals("--bypass-login")) {
            //            m.Show(); l = null;
            //        } else {
            //            l.Show(); m = null;
            //        }
            //    }
            //}

            l.Show();
        }

        /// <summary>
        /// Creates application data directories.
        /// </summary>
        private void CreateDataDirs()
        {
            log.Debug("Creating program data directories...");

            // Build a full path to the directory where the program stores its data
            // The path should be: %APPDATA%\$APP_NAME\data
            string AppDataDir =
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar +                                                        
                AppName + Path.DirectorySeparatorChar + 
                "data";

            // Check if data dir already exists, and if not create it
            /// If it does, just log it
            /// TODO: Should we prompt to overwrite or something here?
            if (!Directory.Exists(AppDataDir)) {
                Directory.CreateDirectory(AppDataDir);
            } else {
                log.Info("Application data directory already exists. Skipping directory creation...");
            }

            if (!Directory.Exists(UserDataDir)) {
                Directory.CreateDirectory(UserDataDir);
            } else {
                log.Info("User data directory already exists. Skipping directory creation...");
            }
        }

        /// <summary>
        /// Creates application SQLite databases.
        /// </summary>
        private void CreateDatabase()
        {
            log.Debug("Creating program database...");

            // Create the application SQLite database
            DatabaseComponent d = new DatabaseComponent();
            d.CreateDatabase();
        }

        /// <summary>
        /// Loads application configuration settings.
        /// </summary>
        private void LoadConfiguration()
        {
            log.Debug("Loading application configuration...");

            // Check if appConfig is populated
            // If yes, load it. If no, populate it.
            try {
                var appConfig = ConfigurationManager.AppSettings;

                if (appConfig.Count == 0) {
                    log.Warn("Application settings not found - informing user and exiting...");
                    MessageBox.Show("Unable to load application configuration information. Please report this issue.");
                    Current.Shutdown();
                } else {
                    log.Info("Application settings found. Loading settings...");

                    foreach (var key in appConfig.AllKeys) {
                        log.Debug(string.Format("Setting: {0}\tValue: {1}", key, appConfig[key]));
                    }
                }
            } catch (ConfigurationErrorsException e) {
                log.Error(string.Format("Configuration error exception! Reason: {0}", e.Message ?? "UNKNOWN"));
            }
        }

        /// <summary>
        /// Returns the program version number in vX.Y.Z format
        /// </summary>
        /// <returns></returns>
        public string GetVersionNumber()
        {
            return string.Format("v{0}.{1}.{2}", MajorVersion, MinorVersion, PatchNumber);
        }

        /// <summary>
        /// Returns the program version string
        /// </summary>
        /// <returns></returns>
        public string GetFullVersionString()
        {
            return string.Format("{0} v{1}.{2}.{3}-{4}", AppName, MajorVersion, MinorVersion, PatchNumber, ReleaseTag);
        }
    }
}
