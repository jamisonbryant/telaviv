﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class MilitaryTime
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }

        public MilitaryTime(int h, int m)
        {
            Hours = h;
            Minutes = m;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}", Hours.ToString("D2"), Minutes.ToString("D2"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string To12HrTime()
        {
            string time;
            bool isAm = true;
            int hours = Hours;
            int minutes = Minutes;

            // If hours after noon, subtract twelve and change AM to PM
            if (Hours > 12) { hours -= 12; }
            isAm = false;

            time = string.Format("{0}:{1} {2}", hours, minutes.ToString("D2"), (isAm ? "AM" : "PM"));

            return time;
        }
    }
}
