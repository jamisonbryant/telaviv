﻿using Bogus;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    /// <summary>
    /// 
    /// </summary>
    public class ValidResponse
    {
        /// <summary>
        /// Is the responder the CDO?
        /// </summary>
        public bool IsCDO { get; set; }

        /// <summary>
        /// Is the responder the Deputy CDO?
        /// </summary>
        public bool IsDeputy { get; set; }

        /// <summary>
        /// Is the responder the first on the scene?
        /// </summary>
        public bool IsFirstOnScene { get; set; }

        /// <summary>
        /// Responder's first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Responder's last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Responder's phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Responder's email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Responder's ETA in military time
        /// </summary>
        public MilitaryTime ETA { get; set; }

        /// <summary>
        /// Overwrites object data with fake data (for demoing/testing purposes)
        /// </summary>
        /// <returns>A fake valid response</returns>
        public ValidResponse Fake(Incident incident)
        {
            Faker faker = new Faker();
            Volunteer v = new Volunteer().Fake();

            IsCDO = false;
            IsDeputy = false;
            IsFirstOnScene = false;
            FirstName = v.FirstName;
            LastName = v.LastName;
            PhoneNumber = v.PrimaryPhone;
            Email = v.Email;

            // Convert ETA in minutes to military time
            ETA = incident.StartTime;
            var minutes = faker.Random.Int(0, incident.StagingTimeLimit);

            while (ETA.Minutes + minutes > 60)
            {
                ETA.Hours++;
                minutes -= 60;
            }

            ETA.Minutes = minutes;

            return this;
        }
    }
}
