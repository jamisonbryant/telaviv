﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Xps;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Xps.Packaging;
using Emerald.Library.Model;
using System.Configuration;

namespace Emerald.Library.View
{
    /// <summary>
    /// Interaction logic for DispatchWindow.xaml
    /// </summary>
    public partial class DispatchWindow : Window
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// Log file
        /// </summary>
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// (unused)
        /// </summary>
        //private bool notesFileCreated = false;

        /// <summary>
        /// Path to which notes will be saved
        /// </summary>
        private string notesSavePath = "";

        /// <summary>
        /// Path to which notes will be printed
        /// </summary>
        private string notesPrintPath = "";

        /// <summary>
        /// The current ongoing dispatch
        /// </summary>
        private Dispatch activeDispatch;

        /// <summary>
        /// Logged valid responses
        /// </summary>
        private ObservableCollection<ValidResponse> loggedResponses;

        /// <summary>
        /// Logged invalid responses
        /// </summary>
        private ObservableCollection<InvalidResponse> invalidResponses;
        
        /// <summary>
        ///
        /// </summary>
        public DispatchWindow(Dispatch d)
        {
            activeDispatch = d;

            // == If in Demo Mode, populate some sample responses
            if (demoMode)
            {
                loggedResponses = new ObservableCollection<ValidResponse>();

                for (var i = 0; i < 15; i++)
                {
                    ValidResponse r = new ValidResponse();
                    loggedResponses.Add(r.Fake(activeDispatch.Incident));
                }

                invalidResponses = new ObservableCollection<InvalidResponse>();

                for (var i = 0; i < 3; i++)
                {
                    InvalidResponse r = new InvalidResponse();
                    invalidResponses.Add(r.Fake(activeDispatch.Incident));
                }
            }
            // == END DEMO MODE CODE

            InitializeComponent();
            SetupDataGrids();
            UpdateLoginMenu();

            // Write some initial notes to the Notes window
            string initialNotes = string.Format("Incident name: {0}\r\nDescription: {1}\r\nStart time: {2}\r\nEnd time: {3}\r\n" +
                "Staging location: {4}\r\nStaging time limit: {5}\r\nTest? {6}\r\nDispatcher: {7}",
                activeDispatch.Incident.Name,
                activeDispatch.Incident.Description,
                activeDispatch.Incident.StartTime.ToString(),
                activeDispatch.Incident.EndTime.ToString(),
                activeDispatch.Incident.StagingLocation,
                activeDispatch.Incident.StagingTimeLimit,
                (activeDispatch.IsATest ? "Yes" : "No"),
                activeDispatch.Dispatcher.ToString());

            NotesTextBox.Text = initialNotes;
        }

        /// <summary>
        /// Updates the menu bar with the user's login information
        /// </summary>
        private void UpdateLoginMenu()
        {
            // Get dispatcher information from dispatch
            Volunteer user = activeDispatch.Dispatcher;

            // Capture and modify login menu text
            // {{LAST_NAME}} -> user last name, {{FIRST_NAME}} -> user first name
            string text = LoggedInUserName.Text;
            text = text.Replace("{{LAST_NAME}}", user.LastName.ToUpper());
            text = text.Replace("{{FIRST_NAME}}", user.FirstName.ToUpper());

            // Update control text with new string
            LoggedInUserName.Text = text;
        }

        /// <summary>
        /// Binds data grids to their collection elements.
        /// </summary>
        private void SetupDataGrids()
        {
            LoggedResponsesDataGrid.DataContext = loggedResponses;
            LoggedResponsesDataGrid.ItemsSource = loggedResponses;

            InvalidResponsesDataGrid.DataContext = invalidResponses;
            InvalidResponsesDataGrid.ItemsSource = invalidResponses;
        }

        /// <summary>
        /// Inserts the current time into the Notes window at the current caret position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertTimeButton_Click(object sender, RoutedEventArgs e)
        {
            // Capture and re-calculate caret index (where blinky line is)
            var toInsert = TimeHelper.GetCurrentTime();
            var oldIndex = NotesTextBox.CaretIndex;
            var newIndex = NotesTextBox.CaretIndex + toInsert.Length;

            NotesTextBox.Text = NotesTextBox.Text.Insert(NotesTextBox.CaretIndex, toInsert);
            NotesTextBox.CaretIndex = newIndex;

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Inserts the current date into the Notes window at the current caret position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertDateButton_Click(object sender, RoutedEventArgs e)
        {
            // Capture and re-calculate caret index (where blinky line is)
            var toInsert = TimeHelper.GetCurrentDate();
            var oldIndex = NotesTextBox.CaretIndex;
            var newIndex = NotesTextBox.CaretIndex + toInsert.Length;

            NotesTextBox.Text = NotesTextBox.Text.Insert(NotesTextBox.CaretIndex, toInsert);
            NotesTextBox.CaretIndex = newIndex;

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Inserts the current timestamp into the Notes window at the current caret position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertTimestampButton_Click(object sender, RoutedEventArgs e)
        {
            // Capture and re-calculate caret index (where blinky line is)
            var toInsert = TimeHelper.GetCurrentTimestamp();
            var oldIndex = NotesTextBox.CaretIndex;
            var newIndex = NotesTextBox.CaretIndex + toInsert.Length;

            NotesTextBox.Text = NotesTextBox.Text.Insert(NotesTextBox.CaretIndex, toInsert);
            NotesTextBox.CaretIndex = newIndex;

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Inserts the current dispatcher's name (last, first) into the Notes window at the current caret position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InsertDispatcherNameButton_Click(object sender, RoutedEventArgs e)
        {
            // Capture and re-calculate caret index (where blinky line is)
            var toInsert = LoggedInUserName.Text;
            var oldIndex = NotesTextBox.CaretIndex;
            var newIndex = NotesTextBox.CaretIndex + toInsert.Length;

            NotesTextBox.Text = NotesTextBox.Text.Insert(NotesTextBox.CaretIndex, toInsert);
            NotesTextBox.CaretIndex = newIndex;

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Clears all notes from the Notes window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearNotesButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to clear notes?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes) {
                NotesTextBox.Text = "";
            }

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Prints the notes currently contained within the Notes window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintNotesButton_Click(object sender, RoutedEventArgs e)
        {
            // Create the print dialog object and set options
            // This code is copy-pasted nearly verbatim from the .NET docs:
            // https://msdn.microsoft.com/en-us/library/aa970848(v=vs.110).aspx
            PrintDialog dialog = new PrintDialog() {
                PageRangeSelection = PageRangeSelection.AllPages,
                UserPageRangeEnabled = true
            };

            notesPrintPath = string.Format("{0}{1}{2}{3}.xps",
                App.UserDataDir,
                System.IO.Path.DirectorySeparatorChar,
                "Dispatch_Notes_",
                TimeHelper.GetCurrentTimestamp().Replace("T", "_").Replace(":", "")
            );

            // Display the dialog (returns true if the user presses the Print button)
            if (dialog.ShowDialog() == true)
            {
                // This will only be reached if the user elects to print the notes via
                // Microsoft's "XML Paper Specification" (XPS), a sh&tty alternative to
                // PDF. This was almost certainly a mistake, and even if it wasn't we
                // don't support XPS printing at this time. Display an error message and return.
                MessageBox.Show("XPS printing is not supported at this time. Please choose another print method.",
                    "Invalid Print Method", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Saves the notes in the Notes window to a file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveNotesButton_Click(object sender, RoutedEventArgs e)
        {
            //if (!notesFileCreated) {
            //    SaveFileDialog dialog = new SaveFileDialog() {
            //        Filter = "Text Files(*.txt)|*.txt|All(*.*)|*",
            //        InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            //    };

            //    // If Save As dialog is submitted (file path selected and OK clicked), capture file path
            //    // If dialog is cancelled (cancel, X, or other button clicked) terminate method
            //    if (dialog.ShowDialog() == true) {
            //        notesFilePath = dialog.FileName;
            //    } else {
            //        return;
            //    }

            //    notesFileCreated = true;
            //}

            // TODO: Use Save File dialog instead of hard saving of file with set name
            // The commented-out code above is the old implementation of the logic of this
            // method, which performed the standard action of prompting the user to choose
            // a location in which to save their data, then saving it. Unfortunately, this
            // logic was blocking the UI thread horribly for an unknown reason, and has been
            // disabled until the faulty logic can be fixed.

            notesSavePath = string.Format("{0}{1}{2}{3}.txt",
                App.UserDataDir,
                System.IO.Path.DirectorySeparatorChar,
                "Dispatch_Notes_",
                TimeHelper.GetCurrentTimestamp().Replace("T", "_").Replace(":", "")
            );

            // Disable button, change cursor, and add status message
            // Will re-enable when saving to disk complete
            // This operation is pretty snappy if we're hard-saving, but still...
            var oldCursor = Cursor;
            var oldContent = SaveNotesButton.Content;

            SaveNotesButton.Content = "Saving...";
            SaveNotesButton.IsEnabled = false;
            Cursor = Cursors.Wait;

            log.Info(string.Format("Wrote contents of Notes textbox to {0}", notesSavePath));
            StatusBarMessage1.Text = string.Format("Saved notes to {0}", notesSavePath);
            File.WriteAllText(notesSavePath, NotesTextBox.Text.ToString());

            SaveNotesButton.Content = oldContent;
            SaveNotesButton.IsEnabled = true;
            Cursor = oldCursor;

            // Give focus back to notes text box
            NotesTextBox.Focus();
        }

        /// <summary>
        /// Detects if certain keyboard shortcuts are pressed in the Notes text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotesTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            bool sKeyPressed = Keyboard.IsKeyDown(Key.S);
            bool pKeyPressed = Keyboard.IsKeyDown(Key.P);
            bool ctrlKeyPressed = (Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftCtrl));
            //bool altKeyPressed = (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt));

            // If Ctrl+S pressed, simulate a click of the Save button
            if (sKeyPressed && ctrlKeyPressed) {
                SaveNotesButton_Click(sender, e);
                e.Handled = true;                       // Prevents an "s" from being added to the text
            }

            //// If Ctrl+Alt+S pressed, reset notes file data and simulate a click of the Save button
            //// This does not erase the file from disk, but rather causes the application to
            //// "forget" about it, causing it to prompt the user to create a new file
            //if (sKeyPressed && ctrlKeyPressed && altKeyPressed) {
            //    notesFileCreated = false;
            //    notesFilePath = "";
            //    SaveNotesButton_Click(sender, e);
            //    e.Handled = true;                       // Prevents an "s" from being added to the text
            //}

            // If Ctrl+P pressed, simulate a click of the Print button
            if (pKeyPressed && ctrlKeyPressed) {
                PrintNotesButton_Click(sender, e);
                e.Handled = true;                       // Prevents a "p" from being added to the text
            }
        }

        /// <summary>
        /// Terminates the program when this window is closed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }
    }
}
