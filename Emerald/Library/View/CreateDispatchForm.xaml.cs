﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Emerald.Library.Model;

namespace Emerald.Library.View
{
    /// <summary>
    /// Interaction logic for CreateDispatchForm.xaml
    /// </summary>
    public partial class CreateDispatchForm : Window
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> cdoList = new ObservableCollection<string>();

        /// <summary>
        /// 
        /// </summary>
        public CreateDispatchForm()
        {
            InitializeComponent();
            PopulateCDOComboBox();
        }

        /// <summary>
        /// 
        /// </summary>
        private void PopulateCDOComboBox()
        {
            List<Volunteer> cdos = (new CervisComponent()).GetVolunteersInGroup(
                ConfigurationManager.AppSettings["GoTeamGroupName"].ToString());

            foreach (Volunteer cdo in cdos) {
                cdoList.Add(cdo.ToString());
            }

            IncidentCdo.ItemsSource = cdoList;
        }

        /// <summary>
        /// Validates the form
        /// </summary>
        /// <returns>True if all form entries are valid, false otherwise</returns>
        private bool ValidateForm()
        {
            bool valid = true;
            string incidentName = IncidentNameField.Text;
            string incidentDescription = IncidentDescriptionField.Text;

            // Check if incident name is blank
            // If so, set invalid switch to true and update field style
            if (string.IsNullOrEmpty(incidentName)) {
                valid = false;
                FormHelper.MarkRequiredFieldInvalid(IncidentNameField);
            }

            // Check if incident description is blank
            // If so, set invalid switch to true and update field style
            if (string.IsNullOrEmpty(incidentDescription)) {
                valid = false;
                FormHelper.MarkRequiredFieldInvalid(IncidentDescriptionField);
            }

            // Display validation failure warning
            if (!valid) {
                MessageBox.Show("One or more required fields are missing. Please check your inputs and try again.");
            }

            return valid;
        }

        /// <summary>
        /// Validates and submits the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitForm(object sender, RoutedEventArgs e)
        {
            Button b = (Button) sender;

            // Change button appearance to indicate form is working
            b.IsEnabled = false;

            // Check if form is valid
            // If so, continue to authentication
            if (ValidateForm()) {
                // Convert the start and end times to datetime objects
                DateTime startTime = Convert.ToDateTime(IncidentStartTimeField.Value);
                DateTime endTime = Convert.ToDateTime(IncidentEndTimeField.Value);

                // Create new Dispatch object
                Dispatch d = new Dispatch()
                {
                    Dispatcher = (Volunteer) Application.Current.Properties["User"],
                    Incident = new Incident()
                    {
                        Name = IncidentNameField.Text,
                        Description = IncidentDescriptionField.Text,
                        StartTime = new MilitaryTime(startTime.Minute, startTime.Hour),
                        EndTime = new MilitaryTime(endTime.Minute, endTime.Hour),
                        Cdo = null,
                        StagingLocation = StagingLocationField.Text,
                        StagingTimeLimit = (int) StagingTimeLimitField.Value
                    },
                    IsATest = (bool) IsDispatchATest.IsChecked
                };

                // Launch dispatch window with dispatch object
                (new DispatchWindow(d)).Show();
                Close();
            }

            // Revert button appearance
            b.IsEnabled = true;
        }

        /// <summary>
        /// Resets the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetForm(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Closes the form and goes back to the previous window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel(object sender, RoutedEventArgs e)
        {
            (new ActionWindow()).Show();
            Close();
        }
    }
}
